package com.and;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SymbiosisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SymbiosisApplication.class, args);
    }

}
